var iFrameLoaded = false;
var minSplashTime = 5000;
var maxSplashTime = 10000;
var delayedStartPerformed = false;
var minSplashTimePassed = false;
var url = "http://www.robohold.com/mobile/?app=true&skip_splash=true&v=1.3"; // "http://www.robohold.com/mobile/?app=true&skip_splash=true&v=1.3";

/*******************************************************************************
 * 
 ******************************************************************************/
/*
 function handleOpenURL(u) {
 setTimeout(function () {
 alert("handleOpenURL(" + u + ")");
 }, 1000);
 if (u.indexOf("loopr://?g=") != -1) {
 var appUrl = "http://direct.loopr.me/?g=" + u.substring(11) + "&app=true";
 url = appUrl;
 document.getElementById("ifr").src = url;
 }
 }
 */
/*******************************************************************************
 * 
 ******************************************************************************/
function removeSplash() {
    if (!minSplashTimePassed) {
        setTimeout(function () {
            removeSplash();
        }, 100);
        return;
    }
    document.getElementById("splash").style.opacity = "0";
    setTimeout(function () {
        document.getElementById("splash").style.display = "none";
    }, 1000);

    /*
     document.body.style.backgroundColor = "#44484b";
     */
}
/*******************************************************************************
 * 
 ******************************************************************************/
function checkIframeState() {
    if (iFrameLoaded) {
        removeSplash();
        return;
    }
    setTimeout(function () {
        checkIframeState();
    }, 100);
}
/*******************************************************************************
 * 
 ******************************************************************************/
function clearCache() {
    try {
        if ((window.cache != undefined) && (window.cache != null)) {
            window.cache.clear();
        }
    } catch (err) {
        console.log("Error in clearCache(): " + err.message);
    }
    delayedStart();
}
/*******************************************************************************
 * 
 ******************************************************************************/
function delayedStart() {
    if (delayedStartPerformed) {
        return;
    }
    delayedStartPerformed = true;
    document.getElementById("ifr").src = url;
    document.getElementById("ifr").onload = function () {
        iFrameLoaded = true;
    };
    setTimeout(function () {
        checkIframeState();
    }, Math.max(0, minSplashTime - 1000));
}
/*******************************************************************************
 * 
 ******************************************************************************/
function docLoaded() {
    //console.log("docLoaded()");

    setTimeout(function () {
        clearCache();
    }, 50);
    setTimeout(function () {
        delayedStart();
    }, 500);
    setTimeout(function () {
        removeSplash();
    }, maxSplashTime);
    setTimeout(function () {
        minSplashTimePassed = true;
    }, minSplashTime);

    //document.getElementById("splash_image").style.opacity = "1";
    document.getElementById("splash_image").onload = function () {
        document.getElementById("splash_image").style.opacity = "1";
    };
    setTimeout(function () {
        document.getElementById("splash_image").style.opacity = "1";
    }, 200);

    document.addEventListener("backbutton", onBackButton, false);
    document.addEventListener("deviceready", function () {
        document.addEventListener("backbutton", onBackButton, false);
    }, false);
}
/*******************************************************************************
 * 
 ******************************************************************************/
function onBackButton() {
    alert("onBackButton()");
    document.getElementById("ifr").src = url + "#back";
    return true;
}

